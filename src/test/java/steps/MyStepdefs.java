package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class MyStepdefs {

  private static final String BASE_URL = "https://api.punkapi.com/v2/beers/";
  private static Response customResponse, response;
  private static String jsonString;

  @Given("user get list of beers with number data {int}")
  public void userGetListOfBeersWithNumberDataNumber_data(int numberData) {
    RestAssured.baseURI = BASE_URL;
    RequestSpecification request = given();
    customResponse = request.get("/?page=2&per_page=" + numberData);
  }

  @And("user validate total amount data expect {int}")
  public void userValidateTotalAmountDataExpectNumber_data(int numberData) {
    jsonString = customResponse.asString();
    List<Map<String, String>> beers = JsonPath.from(jsonString).get("id");
    System.out.println(beers.size());
    Assert.assertTrue(beers.size() == numberData);
  }

  @And("user can see list of beers")
  public void userCanSeeListOfBeers() {
    customResponse.prettyPrint();
  }

  @Given("user get list of beers")
  public void userGetListOfBeers() {
    RestAssured.baseURI = BASE_URL;
    RequestSpecification request = given();
    response = request.get();
  }

  @And("user can see list of name")
  public void userCanSeeListOfName() {
    jsonString = response.asString();
    List<Map<String, String>> listName = JsonPath.from(jsonString).get("name");
    System.out.println(listName);
  }

  @When("user validate total amount data")
  public void userValidateTotalAmountData() {
    jsonString = response.asString();
    List<Map<String, String>> beers = JsonPath.from(jsonString).get("id");
    System.out.println(beers.size());
    Assert.assertTrue(beers.size() == 25);
  }

  @Then("user validate json schema of response")
  public void userValidateJsonSchemaOfResponse() {
    RestAssured.baseURI = BASE_URL;
    given().log().all()
            .contentType(ContentType.JSON)
            .get()
            .then()
            .assertThat()
            .statusCode(200).and().body(matchesJsonSchemaInClasspath("schema/beer.json"));
  }
}
