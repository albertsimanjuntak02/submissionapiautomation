Feature: List of beers

  Scenario Outline: User get list of beers
    Given user get list of beers with number data <number_data>
    And user can see list of beers
    Then user validate total amount data expect <number_data>

    Examples:
      | number_data |
      | 20          |
      | 5           |
      | 1           |

  Scenario: User validate schema and data list of beers
    Given user get list of beers
    And user can see list of name
    When user validate total amount data
    Then user validate json schema of response




