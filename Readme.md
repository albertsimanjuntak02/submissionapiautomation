**Dependencies :**
1. java version "1.8.0_321"
2. Gherkin
3. Cucumber
4. Git
5. Maven
6. Intellij

**Setup:**
1. Git clone this repository
2. Build using maven
3. Run scenario .feature


**Result:**


![Screen_Recording_2022-09-15_at_22.15.03__3_](/uploads/9e44510674b681af5bb1e114282836b6/Screen_Recording_2022-09-15_at_22.15.03__3_.mov)

